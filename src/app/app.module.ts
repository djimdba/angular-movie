import { BrowserModule } from '@angular/platform-browser';
import { NgModule, LOCALE_ID } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule, JsonpModule, RequestOptions } from '@angular/http';
import { RouterModule, Routes, Router } from '@angular/router';
import { AppComponent } from '@ngxmovie/app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';

import { MoviesService , EvenBetterLogger, ConfigService, Logger} from '@ngxmovie/services';
import {SharedModule} from '@ngxmovie/shared';
import { AppRoutingModule } from './app-routing.module';

import { CoreModule, ApiService, APP_CONFIG, APP_DI_CONFIG } from '@ngxmovie/core';


@NgModule({

  imports: [
    BrowserAnimationsModule,
    BrowserModule,
    FormsModule,
    HttpModule,
    JsonpModule,
CoreModule,
SharedModule,
AppRoutingModule

  ],
   declarations: [
    AppComponent
  ],
  providers: [
    { provide: LOCALE_ID, useValue: 'fr'},
    EvenBetterLogger,
    Logger,
    // { provide: Logger, useExisting: EvenBetterLogger },
    ApiService,
    ConfigService,
    MoviesService,
{ provide: APP_CONFIG, useValue: APP_DI_CONFIG }
 ],
  bootstrap: [AppComponent]
})
export class AppModule {
    // Diagnostic only: inspect router configuration
  constructor(router: Router) {
    console.log('Routes: ', JSON.stringify(router.config, undefined, 2));
  }
 }
