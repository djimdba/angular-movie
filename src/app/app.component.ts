import { Component} from '@angular/core';
import {MoviesService} from '@ngxmovie/services';
import {Genres} from '@ngxmovie/model';

@Component({
  selector: 'ngxmovie-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  genres: Array<Genres>;

  constructor(private _moviesServices: MoviesService) {
    this._moviesServices.getGenres().subscribe(res => {
      this.genres = res.genres;
    });
  }

}
