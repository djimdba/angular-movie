import { LangComponent } from './lang/lang.component';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { ApiService } from './api.service';
import { CoreRoutingModule } from './core.routing.module';
import { PageNotFoundComponent } from './pagenotfound.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BaseRequestOptions } from '@angular/http';
import { SwitchThemeComponent } from './themes/switch-theme/switch-theme.component';


@NgModule({
  imports: [
    CommonModule,
    CoreRoutingModule,
    BsDropdownModule.forRoot()
  ],
  declarations: [PageNotFoundComponent, SwitchThemeComponent, LangComponent],
  providers : [ApiService],
  exports: [ PageNotFoundComponent, SwitchThemeComponent, LangComponent]

})
export class CoreModule { }


