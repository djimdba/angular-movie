import { Injectable, Inject, LOCALE_ID } from '@angular/core';
import {Jsonp, URLSearchParams} from '@angular/http';
import 'rxjs/Rx';
import { Logger } from '@ngxmovie/services';
import { APP_CONFIG } from './core.config';

@Injectable()
export class ApiService {
  apikey: string;
  baseUrl: string;
  language: string;
  discover: string;
  movie: string;
  callbackJSON: string;
  constructor(public _jsonp: Jsonp, @Inject(LOCALE_ID) public locale: string, public _Logger: Logger,
   @Inject(APP_CONFIG) public config) {

    this.apikey =  this.config.APIKEY;
    this.baseUrl = this.config.apiEndpoint;
    this.language  = 'language=' + this.locale;
    this.discover = 'discover/';
    this.movie = 'movie?';
  }

  public SearchGet(search: URLSearchParams, url: string) {
     search.set('api_key', this.apikey);
    search.set('language', this.locale);
      search.set('callback', 'JSONP_CALLBACK');
    return this._jsonp.get(url, {search})
      .map(res => {
        this._Logger.log(url);
        return res.json();
      });
  }

}
