import { MoviesComponent } from './movies.component';
import { GenresComponent } from './genres/genres.component';
import { MovieComponent } from './movie/movie.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UpcomingComponent } from './upcoming/upcoming.component';


const routes: Routes = [
  {path: '', component: MoviesComponent},
  {path: 'upcoming', component: UpcomingComponent},
  {path: ':id', component: MovieComponent},
  {path: 'genres/:id/:name', component: GenresComponent}
];

@NgModule({
 imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MoviesRoutingModule {}
