import { ActorSearchComponent } from './actor-search.component';
import { ActorComponent } from './actor.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';



const routes: Routes = [
//  { path: '', redirectTo: 'list', pathMatch: 'full' },
//  { path: 'list', component: ActorComponent },
    { path: ':id',
    component: ActorComponent,
    children: [
{ path: 'detail', component: ActorSearchComponent }
    ] }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ActorRoutingModule {}
