import { Injectable, Inject, LOCALE_ID } from '@angular/core';
import { APP_CONFIG, ApiService } from '@ngxmovie/core';
import { Jsonp , URLSearchParams} from '@angular/http';
import { Logger } from '@ngxmovie/services';
@Injectable()
export class ActorService  {

  constructor(public _ApiService: ApiService,  @Inject(LOCALE_ID) public locale: string, public _jsonp: Jsonp) {

   }
  getPersonDetail(id: string) {
    console.log(id);
    const search = new URLSearchParams();
    const url = this._ApiService.baseUrl + 'person/' + id ;
    return this._ApiService.SearchGet(search, url);
  }

  getPersonCast(id: string) {
    console.log(id);
    const search = new URLSearchParams();
    search.set('api_key', this._ApiService.apikey);
    search.set('language', this.locale);
      search.set('callback', 'JSONP_CALLBACK');
    return this._jsonp.get(this._ApiService.baseUrl  + 'person/' + id + '/movie_credits', {search})
      .map(res => {
        res.json().cast.sort((a, b) => {
        return a.release_date > b.release_date ? -1 : 1;
     });
    console.log(res);
    return res.json();

      });
  }
}
