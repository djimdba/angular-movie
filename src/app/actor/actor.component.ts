import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { ActorService } from './actor.service';

@Component({
  selector: 'ngxmovie-actor',
  templateUrl: './actor.component.html',
  styleUrls: ['./actor.component.css']
})
export class ActorComponent implements OnInit {
  person: Object;
  movies: Array<Object>;
  constructor(private _ActorService: ActorService, private router: ActivatedRoute ) {

  }

  ngOnInit() {

//       this.router.params
//     .switchMap((params: Params) => this._ActorService.getPersonDetail(params['id']))
//     .subscribe((person: Object) => this.person = person);

//     this.router.params
// .switchMap((params: Params) => this._ActorService.getPersonCast(params['id']))
//     .subscribe((res: any) => this.movies = res.cast);
    this.router.paramMap.subscribe((params) => {
      const id = params.get('id');
      console.log(id);
      console.log('message');
      this._ActorService.getPersonDetail(id).subscribe(person => {
        this.person = person;
      });
      this._ActorService.getPersonCast(id).subscribe(res => {
        console.log(res);
        this.movies = res.cast;

      });
    });
  }

}
