/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { ActorComponent } from './actor.component';

import { Router, Routes } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { MovieCardComponent } from 'app/shared/movie-card/movie-card.component';

export const fake_routes: Routes = [
{path: '', component: ActorComponent},
];
class MoviesService {

}
const MockMovieService = jasmine.createSpyObj('MoviesService', ['getPersonDetail']);
describe('ActorComponent', () => {
  let component: ActorComponent;
  let fixture: ComponentFixture<ActorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActorComponent, MovieCardComponent ],
         imports: [RouterTestingModule.withRoutes(fake_routes)],
         providers: [
{ provide: MoviesService, useClass: MockMovieService }
]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
