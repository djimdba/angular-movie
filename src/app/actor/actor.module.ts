import { ActorService } from './actor.service';
import { ActorSearchComponent } from './actor-search.component';
import { ActorComponent } from './actor.component';
import { ActorRoutingModule } from './actor-routing.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ApiService, CoreModule } from '@ngxmovie/core';
import { SharedModule } from '@ngxmovie/shared';



console.log('`Detail` bundle loaded asynchronously');


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    SharedModule,
    CoreModule,
    ActorRoutingModule
  ],
  declarations: [
ActorComponent, ActorSearchComponent],
providers: [ActorService, ApiService],
})
export class ActorModule { }
