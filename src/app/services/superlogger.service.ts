import { Injectable, Inject, LOCALE_ID } from '@angular/core';
import {Jsonp, URLSearchParams} from '@angular/http';
import 'rxjs/Rx';
import { Logger } from './logger.service';
import { ConfigService } from './config.service';


@Injectable()
export class EvenBetterLogger extends Logger {
  constructor(private ConfigService: ConfigService) { super(); }

  log(message: string) {
    const apiVersion = this.ConfigService.apiVersion;
    super.log(`Log me  ${apiVersion}: ${message}`);
  }
}
