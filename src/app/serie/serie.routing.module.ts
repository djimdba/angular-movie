import { SerieComponent } from './serie.component';
import { PopularSeriesComponent } from './popular-series/popular-series.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
    { path: 'popular',
    component: PopularSeriesComponent,
    },
    {path: ':id', component: SerieComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SerieRoutingModule {}
