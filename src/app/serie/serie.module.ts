import { CommonModule } from '@angular/common';
import { SerieRoutingModule } from './serie.routing.module';
import { SerieComponent } from './serie.component';
import { PopularSeriesComponent } from './popular-series/popular-series.component';
import { NgModule } from '@angular/core';

@NgModule({
  imports: [
    CommonModule,
    SerieRoutingModule
  ],
  declarations: [PopularSeriesComponent, SerieComponent]
})
export class SerieModule {}
